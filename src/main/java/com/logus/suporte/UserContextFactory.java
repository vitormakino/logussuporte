/**
 * 
 */
package com.logus.suporte;


import org.springframework.stereotype.Component;

import logus.userContext.UserContext;
import logus.userContext.UserContextDefinition;
import logus.userContext.UserContextDefinitionItem;
import logus.userContext.UserContextItem;


/**
 * @author Makino
 *
 */
@Component("userContextFactory")
public class UserContextFactory {
  
  public UserContext getUserContext(String ano) {
    UserContextDefinition userContextDefinition =
      new UserContextDefinition();

    userContextDefinition.addItem(new UserContextDefinitionItem("COD_CLIENTE_CTX",
                                                                UserContextDefinitionItem.TP_CHARACTER));

    UserContext userContext = new UserContext(userContextDefinition);
    userContext.addItem(new UserContextItem("ANO_EXERCICIO_CTX",
                                            UserContextDefinitionItem.TP_INTEGER,
                                            Integer.valueOf(ano)));
    userContext.addItem(new UserContextItem("COD_CLIENTE_CTX",
                                            UserContextDefinitionItem.TP_CHARACTER,
                                            "00001"));

    return userContext;
  }
}
