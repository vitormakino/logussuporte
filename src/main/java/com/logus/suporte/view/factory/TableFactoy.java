/**
 * 
 */
package com.logus.suporte.view.factory;

import com.logus.suporte.view.builder.TableBuilder;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.TableDragMode;

/**
 * @author Makino
 *
 */
public enum TableFactoy {
  TELA_CADASTRO {
    @Override
    Table create() {
      Table table  =
        TableBuilder.create()
        .sizeFull()
        .bordeless()
        .noHorizontalLines()
        .compact()
        .selectable(true)
        .build();
      
      table.setColumnCollapsingAllowed(true);
      table.setColumnReorderingAllowed(true);
      table.setSortAscending(true);
      table.setDragMode(TableDragMode.MULTIROW);
      table.setMultiSelect(true);
      table.setImmediate(true);
      
      return table;
    }
  };
  
  abstract Table create();
}
