/**
 * 
 */
package com.logus.suporte.view;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;

import com.logus.suporte.view.builder.TableBuilder;
import com.logus.suporte.view.event.LogusEventBus;
import com.logus.suporte.view.helper.ViewHelper;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.VerticalLayout;

import logus.util.model.ValueObject;

/**
 * @author Makino
 *
 */
@SuppressWarnings("serial")
public abstract class BaseCadView<TYPE extends ValueObject>
  extends VerticalLayout implements View {
  
  private Table table;
  
  @PostConstruct
  public void initialize() {
    LogusEventBus.register(this);
    
    setSizeFull();
    setMargin(true);
    setSpacing(true);
    
    Component table = buildTable();
    addComponents(buildHeader(), table);
    setExpandRatio(table, 1);
  }
  
  private Component buildHeader() {
    return ViewHelper.buildTitleViewHeader(this);
  }
  
  private Table buildTable() {
    table = 
      TableBuilder.create()
                  .sizeFull()
                  .bordeless()
                  .noHorizontalLines()
                  .compact()
                  .selectable(true)
                  .columnsFromUIMetadata(getBeanClass())
                  .build();
    
    table.setColumnCollapsingAllowed(true);
    table.setColumnCollapsible("id", false);

    table.setColumnReorderingAllowed(true);
    table.setSortContainerPropertyId("id");
    table.setSortAscending(true);

    // Allow dragging items to the reports menu
    table.setDragMode(TableDragMode.MULTIROW);
    table.setMultiSelect(true);
    table.setImmediate(true);

    return table;
  }
  
  private Collection<TYPE> loadList() {
    try {
      return this.loadAll();
    }
    catch (Exception e) {
      e.printStackTrace();
      return new ArrayList<>();
    }
  }
  
  /* 
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
    for(TYPE vo : loadList()) {
      table.getContainerDataSource().addItem(vo);
    }
  }
  
  protected abstract Class<TYPE> getBeanClass();
  protected abstract Collection<TYPE> loadAll() throws Exception;
}

