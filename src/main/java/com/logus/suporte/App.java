package com.logus.suporte;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;


@SpringBootApplication
@ImportResource({
   "classpath:connections.xml"
  ,"classpath:daos.xml" })
public class App {
  
  public static void main(String[] args)
    throws Exception {
    SpringApplication.run(App.class, args);
  }
}
