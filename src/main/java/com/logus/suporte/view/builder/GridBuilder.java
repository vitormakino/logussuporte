/**
 * 
 */
package com.logus.suporte.view.builder;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.themes.ValoTheme;

import logus.util.adapters.ViewColumn;
import logus.util.annotations.UIMetadata;

/**
 * @author Makino
 *
 */
public class GridBuilder {
  protected GridBuilder(){}
  
  private Grid grid = new Grid();
  
  public static GridBuilder create() {
    return new GridBuilder();
  }
  
  public GridBuilder logusDefault() {
    bordeless();
    compact();
    noHorizontalLines();
    sizeFull();
    return this;
  }
  
  public Grid build() {
    return grid;
  }
  
  public GridBuilder width(float width, Unit unit) {
    grid.setWidth(width, unit);
    return this;
  }
  
  public GridBuilder height(float height, Unit unit) {
    grid.setHeight(height, unit);
    return this;
  }
  
  public GridBuilder sizeFull() {
    grid.setSizeFull();
    return this;
  }
  
  public GridBuilder bordeless() {
    addStyleName(ValoTheme.TABLE_BORDERLESS);
    return this;
  }
  
  public GridBuilder compact() {
    addStyleName(ValoTheme.TABLE_COMPACT);
    return this;
  }
  
  public GridBuilder noHorizontalLines() {
    addStyleName(ValoTheme.TABLE_NO_HORIZONTAL_LINES);
    return this;
  }
  
  public GridBuilder addStyleName(String style) {
    grid.addStyleName(style);
    return this;
  }
  
  public <BEANTYPE> GridBuilder setContainer(Class<? super BEANTYPE> type,
                                  Collection<? extends BEANTYPE> collection) {
    Indexed container = new BeanItemContainer<BEANTYPE>(type, collection);
    grid.setContainerDataSource(container);
    return this;
  }
  
  public GridBuilder fromUIMetadata(Class<?> clazz) {
    List<ViewColumn> _columns = new ArrayList<ViewColumn>();
    for (Method m: clazz.getMethods()) {
      if (m.isAnnotationPresent(UIMetadata.class)) {
        ViewColumn column = new ViewColumn();
        column.setDescription(m.getAnnotation(UIMetadata.class).description());
        column.setFormat(m.getAnnotation(UIMetadata.class).format());
        if (m.getName().startsWith("is"))
          column.setPropertyName(m.getName().substring(2,
                                                       3).toLowerCase() +
                                 m.getName().substring(3));
        else if (m.getName().startsWith("get"))
          column.setPropertyName(m.getName().substring(3,
                                                       4).toLowerCase() +
                                 m.getName().substring(4));

        column.setTitle(m.getAnnotation(UIMetadata.class).label());
        //Se a coluna for java.sql.Date, utiliza-se java.util.Date, devido a
        //um bug apresentado na API (Painel de filtro nao funciona corretamente)
        if (java.sql.Date.class.equals(m.getReturnType())) {
          column.setType(java.util.Date.class);
        }
        else if (Long.class.equals(m.getReturnType())) {
          column.setType(java.lang.String.class);
        }
        else {
          column.setType(m.getReturnType());
        }
        column.setWidth(m.getAnnotation(UIMetadata.class).length());
        column.setPosition(m.getAnnotation(UIMetadata.class).position());
        column.setHiddenColumn(m.getAnnotation(UIMetadata.class).hiddenColumn());
        column.setSearchable(m.getAnnotation(UIMetadata.class).searchable());
        column.setLookupable(m.getAnnotation(UIMetadata.class).lookupable());

        _columns.add(column);
      }
    }
    Collections.sort(_columns, new Comparator<ViewColumn>() {

        @Override
        public int compare(ViewColumn viewColumn,
                           ViewColumn viewColumn1) {
          int pos = viewColumn.getPosition();
          int pos1 = viewColumn1.getPosition();
          return pos == pos1?
                 viewColumn.getTitle().compareTo(viewColumn1.getTitle()):
                 ((Integer) pos).compareTo(pos1);
        }
    });
  
    for (ViewColumn viewColumn : _columns) {
      this.grid.addColumn(viewColumn.getPropertyName(), viewColumn.getType());
      Column column = this.grid.getColumn(viewColumn.getPropertyName());
      column.setHeaderCaption(viewColumn.getTitle());
      column.setHidden(viewColumn.isHiddenColumn());
    }
    
    return this;
  }
}
