/**
 * 
 */
package com.logus.suporte.view.component;


import java.util.Collection;
import java.util.HashSet;

import com.google.common.eventbus.Subscribe;
import com.logus.suporte.view.event.LogusEvent.BrowserResizeEvent;
import com.logus.suporte.view.event.LogusEventBus;
import com.vaadin.server.Page;
import com.vaadin.ui.Table;


/**
 * @author Makino
 *
 */
public class LogusTable extends Table {
  
  private Collection<String> defaultCollapsible = new HashSet<>();
  
  /**
   * 
   */
  public LogusTable() {
    LogusEventBus.register(this);
  }
  
  /**
   * @return retorna valor do atributo {@link defaultCollapsible }
   */
  public Collection<String> getDefaultCollapsible() {
    return defaultCollapsible;
  }

  /**
   * @param defaultCollapsible atualiza atributo {@link defaultCollapsible}
   */
  public void setDefaultCollapsible(Collection<String> defaultCollapsible) {
    this.defaultCollapsible = defaultCollapsible;
  }
  
  /* 
   * {@inheritDoc}
   */
  @Override
  public void setColumnCollapsible(Object propertyId, boolean collapsible) {
    if(!collapsible) {
      this.defaultCollapsible.remove(propertyId);
    }
    super.setColumnCollapsible(propertyId, collapsible);
  }

  private boolean defaultColumnsVisible() {
    boolean collapse = Page.getCurrent().getBrowserWindowWidth() < 800;
    for (String propertyId : defaultCollapsible) {
      if (isColumnCollapsed(propertyId) == collapse) {
        return false;
      }
    }
    return true;
  }

  @Subscribe
  public void browserResized(final BrowserResizeEvent event) {
    // Some columns are collapsed when browser window width gets small
    // enough to make the table fit better.
    if (defaultColumnsVisible()) {
      boolean collapse = 
        Page.getCurrent().getBrowserWindowWidth() < 800;
      
      defaultCollapsible
          .stream()
          .filter(propertyId->isColumnCollapsible(propertyId))
          .forEach(propertyId->setColumnCollapsed(propertyId, collapse));
      
    }
  }
}
