/**
 * 
 */
package com.logus.suporte.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.ibm.db2.jcc.b.id;

import logus.classificacao.model.enumerator.Modulo;

/**
 * @author Makino
 *
 */
@Entity
public class Funcionalidade {
  
  @Id
  private String id;
  
  private String title;
  
  private String icon;
  
  private Modulo modulo;
  
  /**
   * @return retorna valor do atributo {@link id }
   */
  public String getId() {
    return id;
  }
  /**
   * @return retorna valor do atributo {@link title }
   */
  public String getTitle() {
    return title;
  }
  /**
   * @return retorna valor do atributo {@link icon }
   */
  public String getIcon() {
    return icon;
  }
  /**
   * @param id atualiza atributo {@link id}
   */
  public void setId(String id) {
    this.id = id;
  }
  /**
   * @param title atualiza atributo {@link title}
   */
  public void setTitle(String title) {
    this.title = title;
  }
  /**
   * @param icon atualiza atributo {@link icon}
   */
  public void setIcon(String icon) {
    this.icon = icon;
  }
  /**
   * @return retorna valor do atributo {@link modulo }
   */
  public Modulo getModulo() {
    return modulo;
  }
  /**
   * @param modulo atualiza atributo {@link modulo}
   */
  public void setModulo(Modulo modulo) {
    this.modulo = modulo;
  }
}
