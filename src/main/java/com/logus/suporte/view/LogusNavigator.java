/**
 * 
 */
package com.logus.suporte.view;

import com.logus.suporte.view.event.LogusEvent.BrowserResizeEvent;
import com.logus.suporte.view.event.LogusEvent.CloseOpenWindowsEvent;
import com.logus.suporte.view.event.LogusEvent.PostViewChangeEvent;
import com.logus.suporte.view.event.LogusEventBus;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;

/**
 * @author Makino
 *
 */
public class LogusNavigator extends Navigator {

  public LogusNavigator(UI ui, ComponentContainer container) {
    super(ui, container);
    
    addViewChangeListener(new ViewChangeListener() {
      @Override
      public boolean beforeViewChange(ViewChangeEvent event) {
        return true;
      }
      
      @Override
      public void afterViewChange(ViewChangeEvent event) {
        LogusEventBus.post(new PostViewChangeEvent(event.getViewName()));
        LogusEventBus.post(new BrowserResizeEvent());
        LogusEventBus.post(new CloseOpenWindowsEvent());
      }
    });
    
    addViewChangeListener(LogusUI.getEventBus());
  }
  
  
}
