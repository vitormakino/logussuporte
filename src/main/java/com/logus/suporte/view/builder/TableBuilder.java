/**
 * 
 */
package com.logus.suporte.view.builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.logus.suporte.util.ViewHelper;
import com.logus.suporte.view.component.LogusTable;
import com.vaadin.data.Container;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Table;
import com.vaadin.ui.themes.ValoTheme;

import logus.util.adapters.ViewColumn;

/**
 * @author Makino
 *
 */
public class TableBuilder {
  
  private LogusTable table;
  private Container container;
  
  protected TableBuilder(LogusTable table){
    this.table = table;  
  }
  
  public static TableBuilder create() {
    return new TableBuilder(new LogusTable());
  }
  
  public TableBuilder logusDefault() {
    bordeless();
    compact();
    noHorizontalLines();
    return this;
  }
  
  public Table build() {
    return table;
  }
  
  public TableBuilder width(float width, Unit unit) {
    table.setWidth(width, unit);
    return this;
  }
  
  public TableBuilder height(float height, Unit unit) {
    table.setHeight(height, unit);
    return this;
  }
  
  public TableBuilder sizeFull() {
    table.setSizeFull();
    return this;
  }
  
  public TableBuilder bordeless() {
    addStyleName(ValoTheme.TABLE_BORDERLESS);
    return this;
  }
  
  public TableBuilder compact() {
    addStyleName(ValoTheme.TABLE_COMPACT);
    return this;
  }
  
  public TableBuilder noHorizontalLines() {
    addStyleName(ValoTheme.TABLE_NO_HORIZONTAL_LINES);
    return this;
  }
  
  public TableBuilder addStyleName(String style) {
    table.addStyleName(style);
    return this;
  }
  
  public TableBuilder selectable(boolean selectable) {
    table.setSelectable(selectable);
    return this;
  }
  
  public <BEANTYPE> TableBuilder setBeanItemContainer(Class<? super BEANTYPE> type,
                                  Collection<? extends BEANTYPE> collection) {
    container = new BeanItemContainer<BEANTYPE>(type, collection);
    table.setContainerDataSource(container);
    return this;
  }
  
  public <BEANTYPE> TableBuilder columnsFromUIMetadata(Class<BEANTYPE> clazz) {
    List<ViewColumn> _columns = 
      ViewHelper.extractViewColumnFromUIMetadata(clazz);
    
    List<String> visibleColumns = new ArrayList<>();
    List<String> columnsHeaders = new ArrayList<>();
    for (ViewColumn viewColumn : _columns) {
      visibleColumns.add(viewColumn.getPropertyName());
      columnsHeaders.add(viewColumn.getTitle());
    }
    
    if(container == null) {
      container = new BeanItemContainer<>(clazz);
    }
    
    this.table.setContainerDataSource(container);
    this.table.setVisibleColumns(visibleColumns.toArray());
    this.table.setColumnHeaders(columnsHeaders.toArray(new String[columnsHeaders.size()]));
    
    if(visibleColumns.size() > 0 && CollectionUtils.isEmpty(this.table.getDefaultCollapsible())) {
      List<String> defaultColumns =
        visibleColumns
          .stream().filter(propertyId-> this.table.isColumnCollapsible(propertyId))
          .collect(Collectors.toList());
      
      this.table.setDefaultCollapsible(defaultColumns);
    }
    return this;
  }
}
