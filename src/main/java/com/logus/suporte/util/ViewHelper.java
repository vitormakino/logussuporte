/**
 * 
 */
package com.logus.suporte.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import logus.util.adapters.ViewColumn;
import logus.util.annotations.UIMetadata;

/**
 * @author Makino
 *
 */
public class ViewHelper {

  public static List<ViewColumn> extractViewColumnFromUIMetadata(Class<?> clazz) {
    List<ViewColumn> _columns = new ArrayList<ViewColumn>();
    for (Method m: clazz.getMethods()) {
      if (m.isAnnotationPresent(UIMetadata.class)) {
        ViewColumn column = new ViewColumn();
        column.setDescription(m.getAnnotation(UIMetadata.class).description());
        column.setFormat(m.getAnnotation(UIMetadata.class).format());
        if (m.getName().startsWith("is"))
          column.setPropertyName(m.getName().substring(2,
                                                       3).toLowerCase() +
                                 m.getName().substring(3));
        else if (m.getName().startsWith("get"))
          column.setPropertyName(m.getName().substring(3,
                                                       4).toLowerCase() +
                                 m.getName().substring(4));

        column.setTitle(m.getAnnotation(UIMetadata.class).label());
        //Se a coluna for java.sql.Date, utiliza-se java.util.Date, devido a
        //um bug apresentado na API (Painel de filtro nao funciona corretamente)
        if (java.sql.Date.class.equals(m.getReturnType())) {
          column.setType(java.util.Date.class);
        }
        else if (Long.class.equals(m.getReturnType())) {
          column.setType(java.lang.String.class);
        }
        else {
          column.setType(m.getReturnType());
        }
        column.setWidth(m.getAnnotation(UIMetadata.class).length());
        column.setPosition(m.getAnnotation(UIMetadata.class).position());
        column.setHiddenColumn(m.getAnnotation(UIMetadata.class).hiddenColumn());
        column.setSearchable(m.getAnnotation(UIMetadata.class).searchable());
        column.setLookupable(m.getAnnotation(UIMetadata.class).lookupable());

        _columns.add(column);
      }
    }
    
    _columns.sort( (viewColumn,viewColumn1) -> {
          int pos = viewColumn.getPosition();
          int pos1 = viewColumn1.getPosition();
          return pos == pos1?
                 viewColumn.getTitle().compareTo(viewColumn1.getTitle()):
                 ((Integer) pos).compareTo(pos1);
    });
    
    return _columns;
  }
}
