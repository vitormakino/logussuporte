/**
 * 
 */
package com.logus.suporte.view.estruturaclassificatoria;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.logus.suporte.annotation.LogusFuncionalidade;
import com.logus.suporte.model.LogusDAOFactory;
import com.logus.suporte.view.builder.TableBuilder;
import com.logus.suporte.view.event.LogusEventBus;
import com.logus.suporte.view.helper.ViewHelper;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.VerticalLayout;

import logus.classificacao.model.TipoClassificadorVO;
import logus.classificacao.model.persistence.interfaces.TipoClassificadorDAOInterface;

/**
 * @author Makino
 *
 */
@SuppressWarnings("serial")
@SpringView(name="tipo-classificador")
@LogusFuncionalidade(name="Tipo Classificador")
public class TipoClassificadorView extends VerticalLayout implements View {
  
  private TipoClassificadorDAOInterface tipoClassifDao;
  private Table table;
  
  @Autowired
  public TipoClassificadorView(LogusDAOFactory factory) {
    this.tipoClassifDao = factory.getTipoClassificadorDAO();
    
    LogusEventBus.register(this);
    
    setSizeFull();
    setMargin(true);
    setSpacing(true);
    
    Component table = buildTable();
    addComponents(buildHeader(), table);
    setExpandRatio(table, 1);
  }
  
  private Component buildHeader() {
    return ViewHelper.buildTitleViewHeader(this);
  }
  
  private Table buildTable() {
    table = 
      TableBuilder.create()
                  .sizeFull()
                  .bordeless()
                  .noHorizontalLines()
                  .compact()
                  .selectable(true)
                  .columnsFromUIMetadata(TipoClassificadorVO.class)
                  .build();
    
    table.setColumnCollapsingAllowed(true);
    table.setColumnCollapsible("id", false);
    table.setColumnCollapsible("nome", false);

    table.setColumnReorderingAllowed(true);
    table.setSortContainerPropertyId("id");
    table.setSortAscending(true);

    // Allow dragging items to the reports menu
    table.setDragMode(TableDragMode.MULTIROW);
    table.setMultiSelect(true);
    table.setImmediate(true);

    return table;
  }
  
  private List<TipoClassificadorVO> loadAll() {
    try {
      return this.tipoClassifDao.loadAll();
    }
    catch (SQLException e) {
      e.printStackTrace();
      return new ArrayList<>();
    }
  }
  
  /* 
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
    for(TipoClassificadorVO vo : loadAll()) {
      table.getContainerDataSource().addItem(vo);
    }
  }

}
