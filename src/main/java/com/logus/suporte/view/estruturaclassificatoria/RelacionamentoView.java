/**
 * 
 */
package com.logus.suporte.view.estruturaclassificatoria;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import com.logus.suporte.annotation.LogusFuncionalidade;
import com.logus.suporte.view.BaseCadView;
import com.vaadin.spring.annotation.SpringView;

import logus.classificacao.model.RelacionamentoVO;
import logus.classificacao.model.persistence.interfaces.RelacionamentoDAOInterface;

/**
 * @author Makino
 *
 */
@SuppressWarnings("serial")
@SpringView(name="relacionamento")
@LogusFuncionalidade(name="Relacionamento")
public class RelacionamentoView 
  extends BaseCadView<RelacionamentoVO> {

  @Autowired
  private RelacionamentoDAOInterface dao;
  
  /* 
   * {@inheritDoc}
   */
  @Override
  protected Class<RelacionamentoVO> getBeanClass() {
    return RelacionamentoVO.class;
  }

  /* 
   * {@inheritDoc}
   */
  @Override
  protected Collection<RelacionamentoVO> loadAll()
    throws Exception {
    return dao.loadAll();
  }
}