/**
 * 
 */
package com.logus.suporte.controller;


import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import logus.log.model.persistence.interfaces.LogInfoDAOInterface;
import logus.logging.model.LogInfo;


/**
 * @author Makino
 *
 */
@RestController
public class LogInfoController {
  
  //@Autowired
  LogInfoDAOInterface logInfoDao;
  
  @RequestMapping("/loginfo")
  @CrossOrigin(origins = "http://localhost:8888")
  public List<LogInfo> loadById(@RequestParam(value="id") String id) {
    try {
      return logInfoDao.loadById(id);
    }
    catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
