/**
 * 
 */
package com.logus.suporte.view.event;

import logus.logging.model.LogInfo;

/**
 * @author Makino
 *
 */
public class PostLogInfoEvent {
  private LogInfo logInfo;
  
  /**
   * 
   */
  public PostLogInfoEvent(LogInfo logInfo) {
    this.logInfo = logInfo;
  }

  /**
   * @return retorna valor do atributo {@link logInfo }
   */
  public LogInfo getLogInfo() {
    return logInfo;
  }
}
