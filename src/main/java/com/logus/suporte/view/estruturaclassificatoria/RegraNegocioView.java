/**
 * 
 */
package com.logus.suporte.view.estruturaclassificatoria;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import com.logus.suporte.annotation.LogusFuncionalidade;
import com.logus.suporte.view.BaseCadView;
import com.vaadin.spring.annotation.SpringView;

import logus.classificacao.model.RegraNegocioVO;
import logus.classificacao.model.persistence.interfaces.RegraNegocioDAOInterface;

/**
 * @author Makino
 *
 */
@SuppressWarnings("serial")
@SpringView(name="regra-negocio")
@LogusFuncionalidade(name="Regra de Neg�cio")
public class RegraNegocioView 
  extends BaseCadView<RegraNegocioVO> {

  @Autowired
  private RegraNegocioDAOInterface dao;
  
  /* 
   * {@inheritDoc}
   */
  @Override
  protected Class<RegraNegocioVO> getBeanClass() {
    return RegraNegocioVO.class;
  }

  /* 
   * {@inheritDoc}
   */
  @Override
  protected Collection<RegraNegocioVO> loadAll()
    throws Exception {
    return dao.loadAll();
  }
}