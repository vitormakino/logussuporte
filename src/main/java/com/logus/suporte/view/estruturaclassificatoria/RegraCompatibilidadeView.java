/**
 * 
 */
package com.logus.suporte.view.estruturaclassificatoria;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import com.logus.suporte.annotation.LogusFuncionalidade;
import com.logus.suporte.view.BaseCadView;
import com.vaadin.spring.annotation.SpringView;

import logus.classificacao.model.RegraCompatibilidadeVO;
import logus.classificacao.model.persistence.interfaces.RegraCompatibilidadeDAOInterface;

/**
 * @author Makino
 *
 */
@SuppressWarnings("serial")
@SpringView(name="regra-compatibilidade")
@LogusFuncionalidade(name="Regra de Compatibilidade")
public class RegraCompatibilidadeView 
  extends BaseCadView<RegraCompatibilidadeVO> {

  @Autowired
  private RegraCompatibilidadeDAOInterface dao;
  
  /* 
   * {@inheritDoc}
   */
  @Override
  protected Class<RegraCompatibilidadeVO> getBeanClass() {
    return RegraCompatibilidadeVO.class;
  }

  /* 
   * {@inheritDoc}
   */
  @Override
  protected Collection<RegraCompatibilidadeVO> loadAll()
    throws Exception {
    return dao.loadAll();
  }

}