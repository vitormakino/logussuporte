/**
 * 
 */
package com.logus.suporte;

import logus.userContext.UserContext;
import logus.util.integracao.ConnectionInfo;

/**
 * @author Makino
 *
 */
public class LogusContext {
  
  private String descricao;
  private UserContext userContext;
  private ConnectionInfo connection;
  
  /**
   * @return retorna valor do atributo {@link descricao }
   */
  public String getDescricao() {
    return descricao;
  }
  /**
   * @param descricao atualiza atributo {@link descricao}
   */
  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }
  /**
   * @return retorna valor do atributo {@link userContext }
   */
  public UserContext getUserContext() {
    return userContext;
  }
  /**
   * @return retorna valor do atributo {@link connection }
   */
  public ConnectionInfo getConnection() {
    return connection;
  }
  /**
   * @param userContext atualiza atributo {@link userContext}
   */
  public void setUserContext(UserContext userContext) {
    this.userContext = userContext;
  }
  /**
   * @param connection atualiza atributo {@link connection}
   */
  public void setConnection(ConnectionInfo connection) {
    this.connection = connection;
  }
  
  
}
