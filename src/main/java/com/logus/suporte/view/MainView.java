/**
 * 
 */
package com.logus.suporte.view;


import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;


/**
 * @author Makino
 *
 */
@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class MainView extends HorizontalLayout {

  private ComponentContainer content;
  
  @Autowired
  public MainView(MainMenu mainMenu) {
    setSizeFull();

    addComponent(mainMenu);

    this.content = new CssLayout();
    content.addStyleName("view-content");
    content.setSizeFull();
    addComponent(content);
    setExpandRatio(content, 1.0f);
  }
  
  public ComponentContainer getViewContainer() {
    return content;
  }
}
