/**
 * 
 */
package com.logus.suporte.view.helper;

import com.logus.suporte.annotation.LogusFuncionalidade;
import com.vaadin.navigator.View;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.themes.ValoTheme;

/**
 * @author Makino
 *
 */
public class ViewHelper {
  
  public static Component buildTitleViewHeader(View view) {
    String title = view.toString();
    
    LogusFuncionalidade viewTitle = 
      view.getClass().getAnnotation(LogusFuncionalidade.class);
    if(viewTitle != null) {
      title = viewTitle.name();
    }
    
    return buildHeader(title);
  }
  
  public static Component buildHeader(String title) {
    HorizontalLayout header = new HorizontalLayout();
    header.addStyleName("viewheader");
    header.setSpacing(true);

    Label titleLabel = new Label(title);
    titleLabel.setSizeUndefined();
    titleLabel.addStyleName(ValoTheme.LABEL_H1);
    titleLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
    header.addComponent(titleLabel);

    return header;
  }
  
}
