package com.logus.suporte.view.loginfo;


import java.sql.SQLException;

import com.logus.suporte.view.event.LogusEvent.CloseOpenWindowsEvent;
import com.logus.suporte.view.event.LogusEventBus;
import com.vaadin.server.Responsive;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import logus.logging.model.LogInfo;
import logus.logging.model.persistence.PersistenceLogManager;
import logus.logging.model.persistence.PersistenceObjectNode;
import logus.logging.model.persistence.PersistencePropNode;


public class LogInfoWindow
  extends Window {

  private LogInfoWindow(LogInfo info) {
    Responsive.makeResponsive(this);
    setCaption("Detalhes da Informa��o");
    center();
    setResizable(false);
    setClosable(false);
    setHeight(600.0f, Unit.PIXELS);
    setWidth(800.0f, Unit.PIXELS);

    VerticalLayout content = new VerticalLayout();
    content.setSizeFull();
    setContent(content);

    Panel detailsWrapper = new Panel(buildContent(info));
    detailsWrapper.setSizeFull();
    detailsWrapper.addStyleName(ValoTheme.PANEL_WELL);
    content.addComponent(detailsWrapper);
    content.setExpandRatio(detailsWrapper, 1f);
    
    content.addComponent(buildFooter());
  }

  private Component buildFooter() {
    HorizontalLayout footer = new HorizontalLayout();
    footer.addStyleName(ValoTheme.WINDOW_BOTTOM_TOOLBAR);
    footer.setWidth(100.0f, Unit.PERCENTAGE);

    Button ok = new Button("Close");
    ok.addStyleName(ValoTheme.BUTTON_PRIMARY);
    ok.addClickListener(e-> close());
    ok.focus();
    footer.addComponent(ok);
    footer.setComponentAlignment(ok, Alignment.TOP_RIGHT);
    return footer;
  }
  
  public VerticalLayout buildContent(LogInfo info) {
    VerticalLayout layout = new VerticalLayout();
    layout.setWidth(100.0f, Unit.PERCENTAGE);
    layout.addStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
    layout.setSpacing(true);
    layout.setMargin(true);

    final FormLayout form = new FormLayout();
    form.setMargin(false);
    form.setWidth("800px");
    form.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
    layout.addComponent(form);
    layout.setExpandRatio(form, 1);
    
    Label section = new Label("Informa��es");
    section.addStyleName(ValoTheme.LABEL_H2);
    section.addStyleName(ValoTheme.LABEL_COLORED);
    form.addComponent(section);

    TextField codigo = new TextField("C�digo");
    codigo.setWidth("50%");
    codigo.setValue(info.getId());
    form.addComponent(codigo);

    DateField time = new DateField("Hora");
    time.setValue(info.getTime());
    time.setResolution(Resolution.SECOND);
    form.addComponent(time);

    TextField username = new TextField("Usu�rio");
    username.setRequired(true);
    username.setValue(info.getUserId());
    form.addComponent(username);

    TextField tipo = new TextField("Tipo");
    tipo.setWidth("50%");
    tipo.setRequired(true);
    tipo.setValue(info.getDscLogType());
    form.addComponent(tipo);

    section = new Label("Altera��o");
    section.addStyleName(ValoTheme.LABEL_H4);
    section.addStyleName(ValoTheme.LABEL_COLORED);
    form.addComponent(section);

    carregaRootList();
    form.addComponent(ttable);

    form.setReadOnly(true);

    return layout;
  }

  /**
   * Busca os hist�ricos ra�z e monta a lista de TreeNode ra�z para a �rvore de Hist�rico.
   * @return Lista de n�s ra�z.
   * @throws SQLException
   */
  TreeTable ttable;

  public void carregaRootList() {
    ttable = new TreeTable();
    ttable.addContainerProperty("Name", String.class, null);
    ttable.addContainerProperty("Number", Integer.class, null);

    PersistenceObjectNode persistenceObject = null;
    PersistenceLogManager persistenceManager = new PersistenceLogManager();
    String newLogValue = "";//info.getLogValue();

    if (newLogValue != null && newLogValue != "") {
      try {
        persistenceObject = persistenceManager.parse(newLogValue);
        System.out.println(persistenceObject);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void g(int i, int parent, PersistenceObjectNode node) {
    for (PersistencePropNode propNode : node.getPropNode()) {
      ttable.addItem(new Object[] { propNode.getPropName(),
        propNode.getPropValue() }, i);
      ttable.setParent(i, parent);
      for (PersistenceObjectNode component : propNode.getObjectNode()) {
        g(i + 1, i, component);
      }
    }
  }
  
  public static void open(final LogInfo info) {
    LogusEventBus.post(new CloseOpenWindowsEvent());
    Window w = new LogInfoWindow(info);
    UI.getCurrent().addWindow(w);
    w.focus();
  }
}
