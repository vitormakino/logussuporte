/**
 * 
 */
package com.logus.suporte.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logus.suporte.model.entity.Funcionalidade;

/**
 * @author Makino
 *
 */
public interface LogusViewRepository extends JpaRepository<Funcionalidade, String>{
  List<Funcionalidade> findByTitle(String title);
}
