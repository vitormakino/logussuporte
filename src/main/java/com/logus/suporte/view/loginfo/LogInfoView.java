/**
 * 
 */
package com.logus.suporte.view.loginfo;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.logus.suporte.annotation.LogusFuncionalidade;
import com.logus.suporte.model.LogusDAOFactory;
import com.logus.suporte.view.event.LogusEventBus;
import com.logus.suporte.view.event.PostLogInfoEvent;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import logus.classificacao.model.enumerator.Modulo;
import logus.log.model.persistence.interfaces.LogInfoDAOInterface;
import logus.logging.model.LogInfo;

/**
 * @author Makino
 *
 */
@SuppressWarnings("serial")
@SpringView(name="log-info")
@LogusFuncionalidade(name="Informa��es de Log",modulo=Modulo.SIPLAG)
public class LogInfoView extends VerticalLayout implements View {

  private LogInfoDAOInterface repo;
  
  private Grid grid;
  private TextField filter;
  private Button searchBtn;
  private Label titleLabel;
  
  @Autowired
  private LogusDAOFactory factory;
  
  @PostConstruct
  public void initialize() {
    this.repo = factory.getLogInfoDAO();
    this.grid = new Grid();
    this.filter = new TextField();
    this.searchBtn = new Button("Buscar", FontAwesome.SEARCH);

    filter.setInputPrompt("Informe o id");
    // build layout
    addComponents(buildHeader(),grid);

    // Configure layouts and components
    setMargin(true);
    setSpacing(true);

    grid.setSizeFull();
    grid.addStyleName(ValoTheme.TABLE_BORDERLESS);
    grid.addStyleName(ValoTheme.TABLE_COMPACT);
    grid.addStyleName(ValoTheme.TABLE_NO_HORIZONTAL_LINES);
    grid.setColumns("id", "userId", "dscLogType","time");
    grid.getColumn("id").setHeaderCaption("C�digo");
    grid.getColumn("time").setHeaderCaption("Hora");
    grid.getColumn("dscLogType").setHeaderCaption("Tipo");
    grid.getColumn("userId").setHeaderCaption("Usu�rio");
    this.searchBtn.addClickListener(e -> listLogs(this.filter.getValue()));
    
    grid.addSelectionListener(e -> {
      if (!e.getSelected().isEmpty()) {
        LogInfo selectedRow = (LogInfo)grid.getSelectedRow();
        //LogInfoWindow.open(selectedRow);
        LogusEventBus.post(new PostLogInfoEvent(selectedRow));
        UI.getCurrent().getNavigator().navigateTo(LogInfoFormView.VIEW_NAME);
      }
    });

    listLogs(null);
    setExpandRatio(grid, 1);
  }

  private Component buildHeader() {
    HorizontalLayout header = new HorizontalLayout();
    header.addStyleName("viewheader");
    header.setSpacing(true);

    titleLabel = new Label("Informa��es de Log");
    titleLabel.setSizeUndefined();
    titleLabel.addStyleName(ValoTheme.LABEL_H1);
    titleLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
    header.addComponent(titleLabel);

    
    HorizontalLayout tools = new HorizontalLayout(this.filter,
                                                  this.searchBtn);
    tools.setSpacing(true);
    tools.addStyleName("toolbar");
    header.addComponent(tools);
                                          
    return header;
  }
  
  /**
   * 
   */
  private void listLogs(String id) {
    if(!StringUtils.isEmpty(id)) {
      try {
        Indexed container = new BeanItemContainer<LogInfo>(LogInfo.class, repo.loadById(id));
        grid.setContainerDataSource(container);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /* 
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
    
  }
}
