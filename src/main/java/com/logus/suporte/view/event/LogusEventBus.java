/**
 * 
 */
package com.logus.suporte.view.event;

import java.util.Queue;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.collect.Queues;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import com.logus.suporte.view.LogusUI;
import com.vaadin.navigator.ViewChangeListener;

/**
 * @author Makino
 *
 */
@Component
@Scope("prototype")
public class LogusEventBus implements SubscriberExceptionHandler, ViewChangeListener{

  private final EventBus eventBus = new EventBus(this);
  private final Queue<Object> queue = Queues.newConcurrentLinkedQueue();
  
  public static void register(Object obj) {
    LogusUI.getEventBus().eventBus.register(obj);
  }
  
  public static void unregister(Object obj) {
    LogusUI.getEventBus().eventBus.unregister(obj);
  }
  
  public static void post(Object obj) {
    //LogusUI.getEventBus().eventBus.post(obj);
    LogusUI.getEventBus().queue.add(obj);
  }
  
  /* 
   * {@inheritDoc}
   */
  @Override
  public void handleException(Throwable exception,
                              SubscriberExceptionContext context) {
    exception.printStackTrace();
  }

  /* 
   * {@inheritDoc}
   */
  @Override
  public boolean beforeViewChange(ViewChangeEvent event) {
    return true;
  }

  /* 
   * {@inheritDoc}
   */
  @Override
  public void afterViewChange(ViewChangeEvent event) {
    LogusUI.getEventBus().queue.forEach(obj -> LogusUI.getEventBus().eventBus.post(obj));
  }

}
