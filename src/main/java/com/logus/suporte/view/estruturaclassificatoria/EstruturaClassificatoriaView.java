/**
 * 
 */
package com.logus.suporte.view.estruturaclassificatoria;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.eventbus.Subscribe;
import com.logus.suporte.annotation.LogusFuncionalidade;
import com.logus.suporte.view.builder.TableBuilder;
import com.logus.suporte.view.event.LogusEventBus;
import com.logus.suporte.view.event.LogusEvent.BrowserResizeEvent;
import com.logus.suporte.view.helper.ViewHelper;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.VerticalLayout;

import logus.classificacao.model.EstruturaClassificatoriaVO;
import logus.classificacao.model.persistence.interfaces.EstruturaClassificatoriaDAOInterface;


/**
 * @author Makino
 *
 */
@SuppressWarnings("serial")
@SpringView(name = "estrutura-classificatoria")
@LogusFuncionalidade(name = "Estrutura Classificatória")
public class EstruturaClassificatoriaView
  extends VerticalLayout
  implements View {

  private EstruturaClassificatoriaDAOInterface dao;
  private Table table;

  @Autowired
  public EstruturaClassificatoriaView(EstruturaClassificatoriaDAOInterface dao) {
    this.dao = dao;
    setSizeFull();
    setMargin(true);
    setSpacing(true);

    LogusEventBus.register(this);
    
    Component table = buildTable();
    addComponents(buildHeader(), table);
    setExpandRatio(table, 1);
  }

  private Component buildHeader() {
    return ViewHelper.buildTitleViewHeader(this);
  }

  private Table buildTable() {
    table =
      TableBuilder
          .create()
          .sizeFull()
          .bordeless()
          .noHorizontalLines()
          .compact()
          .selectable(true)
          .columnsFromUIMetadata(EstruturaClassificatoriaVO.class)
          .build();

    table.setColumnCollapsingAllowed(true);
    table.setColumnCollapsible("id", false);
    table.setColumnCollapsible("nome", false);

    table.setColumnReorderingAllowed(true);
    table.setSortContainerPropertyId("id");
    table.setSortAscending(true);

    // Allow dragging items to the reports menu
    table.setDragMode(TableDragMode.MULTIROW);
    table.setMultiSelect(true);
    table.setImmediate(true);

    return table;
  }

  private List<EstruturaClassificatoriaVO> loadAll() {
    List<EstruturaClassificatoriaVO> collection = new ArrayList<>();
    try {
      collection = dao.loadAll();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    return collection;
  }

  /*
   * {@inheritDoc}
   */
  @Override
  public void enter(ViewChangeEvent event) {
    for (EstruturaClassificatoriaVO vo : loadAll()) {
      table.getContainerDataSource().addItem(vo);
    }
  }
}
