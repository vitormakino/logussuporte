package com.logus.suporte.view;


import java.util.Collection;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.eventbus.Subscribe;
import com.logus.common.base.Strings;
import com.logus.suporte.model.entity.Funcionalidade;
import com.logus.suporte.model.repository.LogusViewRepository;
import com.logus.suporte.view.event.LogusEvent;
import com.logus.suporte.view.event.LogusEvent.PostViewChangeEvent;
import com.logus.suporte.view.event.LogusEventBus;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import logus.classificacao.model.enumerator.Modulo;
import logus.security.model.UserVO;


@SuppressWarnings("serial")
@SpringComponent
@UIScope
public final class MainMenu
  extends CustomComponent {

  public static final String ID = "logus-suporte-menu";
  private static final String STYLE_VISIBLE = "valo-menu-visible";
  private MenuItem settingsItem;
  private ComboBox searchCb;

  private LogusViewRepository viewRepo;

  @Autowired
  public MainMenu(LogusViewRepository viewRepo) {
    this.viewRepo = viewRepo;
    setPrimaryStyleName(ValoTheme.MENU_ROOT);
    setId(ID);
    setSizeUndefined();
    setCompositionRoot(buildContent());
  }

  @PostConstruct
  public void initialize() {
  //  LogusEventBus.register(this);
  }
  
  private Component buildContent() {
    final CssLayout menuContent = new CssLayout();
    menuContent.addStyleName("sidebar");
    menuContent.addStyleName(ValoTheme.MENU_PART);
    menuContent.addStyleName("no-vertical-drag-hints");
    menuContent.addStyleName("no-horizontal-drag-hints");
    menuContent.setWidth(null);
    menuContent.setHeight("100%");

    menuContent.addComponent(buildTitle());
    menuContent.addComponent(buildUserMenu());
    menuContent.addComponent(buildToggleButton());
    menuContent.addComponent(buildMenuItems(buildSearchView()));
    return menuContent;
  }

  private Component buildTitle() {
    Image logo = new Image(null,new ThemeResource("img/logus.png"));
    HorizontalLayout logoWrapper = new HorizontalLayout(logo);
    logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
    logoWrapper.addStyleName(ValoTheme.MENU_TITLE);
    return logoWrapper;
  }

  private Component buildSearchView() {
    this.searchCb = new ComboBox();
    this.searchCb.setWidth(100.f, Unit.PERCENTAGE);
    this.searchCb.setInputPrompt("Buscar Funcionalidade");
    final BeanItemContainer<Funcionalidade> dataSource =
      new BeanItemContainer<Funcionalidade>(Funcionalidade.class,
                                       this.viewRepo.findAll());
    this.searchCb.setContainerDataSource(dataSource);
    this.searchCb.setItemCaptionMode(ItemCaptionMode.PROPERTY);
    this.searchCb.setItemCaptionPropertyId("title");
    this.searchCb.setFilteringMode(FilteringMode.CONTAINS);
    this.searchCb.setImmediate(true);
    this.searchCb.addStyleName(ValoTheme.COMBOBOX_TINY);
    this.searchCb.addValueChangeListener((e) -> {
      if (e.getProperty() != null) {
        Funcionalidade view =
          (Funcionalidade) e.getProperty().getValue();
        navigateTo(view.getId());
      }
    });

    return this.searchCb;
  }

  private UserVO getCurrentUser() {
    return (UserVO) VaadinSession.getCurrent().getAttribute(
                                                            UserVO.class.getName());
  }

  private Component buildUserMenu() {
    final MenuBar settings = new MenuBar();
    settings.addStyleName("user-menu");
    settingsItem = settings.addItem("", new ThemeResource(
      "img/profile-pic-300px.jpg"), null);
    settingsItem.addItem("Prefer�ncias", selectedItem -> Notification.show("N�o implementado"));
    settingsItem.addItem("Ajuda",  selectedItem -> Notification.show("N�o implementado"));
    settingsItem.addSeparator();
    settingsItem.addItem("Sair", selectedItem ->{
      LogusEventBus.post(new LogusEvent.UserLoggedOutEvent());
    });
    //final UserVO user = getCurrentUser();
    UserVO user = new UserVO("admin", "Administrador");
    settingsItem.setText(user.getNome());
    return settings;
  }

  private Component buildToggleButton() {
    Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
      @Override
      public void buttonClick(final ClickEvent event) {
        if (getCompositionRoot().getStyleName().contains(STYLE_VISIBLE)) {
          getCompositionRoot().removeStyleName(STYLE_VISIBLE);
        }
        else {
          getCompositionRoot().addStyleName(STYLE_VISIBLE);
        }
      }
    });
    valoMenuToggleButton.setIcon(FontAwesome.LIST);
    valoMenuToggleButton.addStyleName("valo-menu-toggle");
    valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
    return valoMenuToggleButton;
  }

  private Component buildMenuItems(Component c) {
    Multimap<Modulo,Funcionalidade> mapa = HashMultimap.create();
    for (Funcionalidade view : viewRepo.findAll()) {
      mapa.put(view.getModulo(), view);
    }
    
    CssLayout menuItemsLayout = new CssLayout();
    menuItemsLayout.addStyleName("valo-menuitems");
   
    c.addStyleName(ValoTheme.MENU_ITEM);
    menuItemsLayout.addComponent(c);
    
    Button principal = new Button("P�gina Inicial");
    principal.setPrimaryStyleName(ValoTheme.MENU_ITEM);
    principal.setIcon(FontAwesome.HOME);
    //LogusEventBus.register(this);
    principal.setHtmlContentAllowed(true);
    principal.addClickListener((e) -> navigateTo(""));
    menuItemsLayout.addComponent(principal);
    
    for (Modulo modulo : mapa.keySet()) {
      Collection<Funcionalidade> collection = mapa.get(modulo);
      int count = collection.size();
      String descricao = modulo.getDescricao() +
        " <span class=\"valo-menu-badge\">" + count + "</span>";
      Label label = new Label(descricao, ContentMode.HTML);
      label.setPrimaryStyleName(ValoTheme.MENU_SUBTITLE);
      label.addStyleName(ValoTheme.LABEL_H4);
      label.setSizeUndefined();
      menuItemsLayout.addComponent(label);

      for (Funcionalidade view : collection) {
        if(Strings.isEmpty(view.getId())) {
          continue;
        }
        Button b = new MainMenuItemButton(view);
        menuItemsLayout.addComponent(b);
      }
    }
    return menuItemsLayout;
  }

  private void navigateTo(String viewId) {
    UI.getCurrent().getNavigator().navigateTo(viewId);
  }
  
  public final class MainMenuItemButton extends Button {

    private static final String STYLE_SELECTED = "selected";

    private final Funcionalidade view;

    public MainMenuItemButton(final Funcionalidade view) {
        this.view = view;
        setPrimaryStyleName(ValoTheme.MENU_ITEM);
        //setIcon(view.getIcon());
        setCaption(view.getTitle());
        //LogusEventBus.register(this);
        setHtmlContentAllowed(true);
        addClickListener((e) -> navigateTo(view.getId()));

    }

    @Subscribe
    public void postViewChange(final PostViewChangeEvent event) {
        removeStyleName(STYLE_SELECTED);
        if (event.getViewName().equals(view.getId())) {
          addStyleName(STYLE_SELECTED);
        }
    }
}
}
