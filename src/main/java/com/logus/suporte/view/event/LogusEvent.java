package com.logus.suporte.view.event;

public class LogusEvent {

  private LogusEvent() {
    
  }
  
  public static class BrowserResizeEvent {

  }

  public static class UserLoggedOutEvent {

  }

  public static class NotificationsCountUpdatedEvent {
  }

  public static final class ReportsCountUpdatedEvent {
  }

  public static final class TransactionReportEvent {
  }

  public static final class PostViewChangeEvent {
    private String viewName;
    
    public PostViewChangeEvent(String viewName) {
      this.viewName = viewName;
    }

    /**
     * @return retorna valor do atributo {@link viewName }
     */
    public String getViewName() {
      return viewName;
    }
  }

  public static class CloseOpenWindowsEvent {
  }

  public static class ProfileUpdatedEvent {
  }

}
