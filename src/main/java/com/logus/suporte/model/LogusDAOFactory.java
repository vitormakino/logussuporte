/**
 * 
 */
package com.logus.suporte.model;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import logus.classificacao.model.persistence.ClassificacaoDAOFactory;
import logus.classificacao.model.persistence.interfaces.EstruturaClassificatoriaDAOInterface;
import logus.classificacao.model.persistence.interfaces.RegraCompatibilidadeDAOInterface;
import logus.classificacao.model.persistence.interfaces.RegraMapeamentoDAOInterface;
import logus.classificacao.model.persistence.interfaces.RegraNegocioDAOInterface;
import logus.classificacao.model.persistence.interfaces.RelacionamentoDAOInterface;
import logus.classificacao.model.persistence.interfaces.TipoClassificadorDAOInterface;
import logus.log.model.persistence.LogDAOFactory;
import logus.log.model.persistence.interfaces.LogInfoDAOInterface;
import logus.userContext.UserContext;

/**
 * @author Makino
 *
 */
@Component
public class LogusDAOFactory {

  @Autowired
  private UserContext userContext;

  public TipoClassificadorDAOInterface getTipoClassificadorDAO() {
    try {
      return ClassificacaoDAOFactory.getDAOFactory(userContext).getTipoClassificadorDAO();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  @Bean
  @Scope("prototype")
  @Lazy
  public EstruturaClassificatoriaDAOInterface getEstruturaClassificatoriaDAO()  {
    try {
      return ClassificacaoDAOFactory.getDAOFactory(userContext).getEstruturaClassificatoriaDAO();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  @Bean
  @Scope("prototype")
  @Lazy
  public RegraNegocioDAOInterface getRegraNegocioDAO()  {
    try {
      return ClassificacaoDAOFactory.getDAOFactory(userContext).getRegraNegocioDAO();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  @Bean
  @Scope("prototype")
  @Lazy
  public RegraCompatibilidadeDAOInterface getRegraCompatibilidadeDAO()  {
    try {
      return ClassificacaoDAOFactory.getDAOFactory(userContext).getRegraCompatibilidadeDAO();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  @Bean
  @Scope("prototype")
  @Lazy
  public RegraMapeamentoDAOInterface getRegraMapeamentoDAO()  {
    try {
      return ClassificacaoDAOFactory.getDAOFactory(userContext).getRegraMapeamentoDAO();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  @Bean
  @Scope("prototype")
  @Lazy
  public RelacionamentoDAOInterface getRelacionamentoDAO()  {
    try {
      return ClassificacaoDAOFactory.getDAOFactory(userContext).getRelacionamentoDAO();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  
  public LogInfoDAOInterface getLogInfoDAO() {
    try {
      return LogDAOFactory.getDAOFactory(userContext).getLogInfoDAO();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
}
