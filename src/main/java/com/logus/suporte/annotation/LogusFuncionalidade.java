/**
 * 
 */
package com.logus.suporte.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import logus.classificacao.model.enumerator.Modulo;

/**
 * @author Makino
 *
 */
@Target({ java.lang.annotation.ElementType.TYPE })
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface LogusFuncionalidade {
  String name() default "";
  Modulo modulo() default Modulo.GERAL;
}
