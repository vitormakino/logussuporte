/**
 * 
 */
package com.logus.suporte.view.estruturaclassificatoria;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import com.logus.suporte.annotation.LogusFuncionalidade;
import com.logus.suporte.view.BaseCadView;
import com.vaadin.spring.annotation.SpringView;

import logus.classificacao.model.RegraMapeamentoVO;
import logus.classificacao.model.persistence.interfaces.RegraMapeamentoDAOInterface;

/**
 * @author Makino
 *
 */
@SuppressWarnings("serial")
@SpringView(name="regra-mapeamento")
@LogusFuncionalidade(name="Regra de Mapeamento")
public class RegraMapeamentoView 
  extends BaseCadView<RegraMapeamentoVO> {

  @Autowired
  private RegraMapeamentoDAOInterface dao;
  
  /* 
   * {@inheritDoc}
   */
  @Override
  protected Class<RegraMapeamentoVO> getBeanClass() {
    return RegraMapeamentoVO.class;
  }

  /* 
   * {@inheritDoc}
   */
  @Override
  protected Collection<RegraMapeamentoVO> loadAll()
    throws Exception {
    return dao.loadAll();
  }
}