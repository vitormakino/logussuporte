/**
 * 
 */
package com.logus.suporte;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.logus.cache2.Cache;
import com.logus.suporte.annotation.LogusFuncionalidade;
import com.logus.suporte.model.entity.Funcionalidade;
import com.logus.suporte.model.repository.LogusViewRepository;
import com.vaadin.spring.annotation.SpringView;

import logus.classificacao.model.persistence.ClassificacaoDAOFactory;
import logus.comunica.model.persistence.ComunicaDAOFactory;
import logus.federalsupply.model.persistence.FederalSupplyDAOFactory;
import logus.log.model.persistence.LogDAOFactory;
import logus.logging.model.persistence.dao.LoggingDAOFactory;
import logus.security.model.persistence.SecurityDAOFactory;
import logus.siplag.model.persistence.SiplagDAOFactory;
import logus.siplag.model.persistence.SiplagExecDAOFactory;
import logus.siplag.model.persistence.SiplagPlanDAOFactory;
import logus.util.integracao.ConnectionInfo;
import logus.util.integracao.SGBDInfo;
import logus.util.model.SchemaDAO;
import logus.workflow.model.persistencia.WorkflowDAOFactory;

/**
 * @author Makino
 *
 */
@Component
public class Initializer implements CommandLineRunner {

  private static final Logger LOGGER = LoggerFactory
          .getLogger(Initializer.class);
  
  @Autowired
  private LogusViewRepository viewRepository;
 
  @Autowired
  private ApplicationContext applicationContext;
  
  @Autowired
  private ConnectionInfo connInfo;
  
  private List<String> contextKeys = Arrays.asList("ANO_EXERCICIO_CTX");
  
  @Autowired
  private SGBDInfo sgbdInfo;
  
  private Cache<Object, Object> cache;
  
  /* 
   * {@inheritDoc}
   */
  @Override
  public void run(String... args)
    throws Exception {
    initViews();
    initDatabaseConnection();
  }
  
  private void initViews() {
    List<Funcionalidade> views =  new ArrayList<>();
    LOGGER.info("Inicializando Views - Inicio");
    final String[] viewBeanNames = applicationContext
            .getBeanNamesForAnnotation(SpringView.class);
    for (String beanName : viewBeanNames) {
      final LogusFuncionalidade viewTitle = applicationContext
        .findAnnotationOnBean(beanName, LogusFuncionalidade.class);
      if(viewTitle == null) {
        continue;
      }
      
      final SpringView springView = applicationContext
        .findAnnotationOnBean(beanName, SpringView.class);
        
        Funcionalidade view = new Funcionalidade();
        view.setId(springView.name());
        view.setTitle(viewTitle.name());
        view.setModulo(viewTitle.modulo());
        view.setIcon(String.valueOf(Math.random() * 100));
        views.add(view);
    }

    viewRepository.save(views);
    LOGGER.info("Inicializando Views - Fim");
  }
  
  /**
   * Inicia a conex�o das f�bricas de DAO com o banco.
   */
  private void initDatabaseConnection() {
    SecurityDAOFactory.setConnectionInfo(connInfo);
    SecurityDAOFactory.setCacheInstance(cache);

    SiplagDAOFactory.setConnectionInfo(connInfo);
    SiplagDAOFactory.setAppendContextKeys(contextKeys);
    SiplagDAOFactory.setCacheInstance(cache);

    SiplagPlanDAOFactory.setConnectionInfo(connInfo);
    SiplagPlanDAOFactory.setAppendContextKeys(contextKeys);
    SiplagPlanDAOFactory.setCacheInstance(cache);

    SiplagExecDAOFactory.setConnectionInfo(connInfo);
    SiplagExecDAOFactory.setAppendContextKeys(contextKeys);
    SiplagExecDAOFactory.setCacheInstance(cache);
    
    LogDAOFactory.setConnectionInfo(connInfo);
    LogDAOFactory.setAppendContextKeys(contextKeys);
    LogDAOFactory.setCacheInstance(cache);

    LoggingDAOFactory.setConnectionInfo(connInfo);
    LoggingDAOFactory.setCacheInstance(cache);
    try {
      // Configura o LoggingDAO para que o log seja feito com conte�do
      // compactado.
      LoggingDAOFactory.getDAOFactory().getLoggingDAO().setZipLogValue(true);
    }
    catch (SQLException e) {
      e.printStackTrace();
    }

    FederalSupplyDAOFactory.setConnectionInfo(connInfo);
    FederalSupplyDAOFactory.setAppendContextKeys(contextKeys);
    FederalSupplyDAOFactory.setCacheInstance(cache);

    ComunicaDAOFactory.setConnectionInfo(connInfo);
    ComunicaDAOFactory.setCacheInstance(cache);

    WorkflowDAOFactory.setConnectionInfo(connInfo);
    WorkflowDAOFactory.setAppendContextKeys(contextKeys);
    WorkflowDAOFactory.setCacheInstance(cache);

    ClassificacaoDAOFactory.setConnectionInfo(connInfo);
    ClassificacaoDAOFactory.setAppendContextKeys(contextKeys);
    ClassificacaoDAOFactory.setCacheInstance(cache);
    
    SchemaDAO.getInstance().setSgbdInfo(this.sgbdInfo);
  }
}
