package com.logus.suporte.view.loginfo;


import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.eventbus.Subscribe;
import com.logus.suporte.view.event.LogusEventBus;
import com.logus.suporte.view.event.PostLogInfoEvent;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import logus.logging.model.LogInfo;
import logus.logging.model.persistence.PersistenceLogManager;
import logus.logging.model.persistence.PersistenceObjectNode;
import logus.logging.model.persistence.PersistencePropNode;

@SpringView(name=LogInfoFormView.VIEW_NAME)
public class LogInfoFormView
  extends VerticalLayout
  implements View {

  public final static String VIEW_NAME = "log-info-form";
  private TextField codigo;
  private DateField time;
  private TextField tipo;
  private TextField username;
  
  @PreDestroy
  public void destroy() {
    System.out.println("DESTROY");
    LogusEventBus.unregister(this);
  }
  
  @PostConstruct
  public void init() {
    System.out.println("Registra");
    LogusEventBus.register(this);
  }
  
  public LogInfoFormView() {
    setSpacing(true);
    setMargin(true);

    final FormLayout form = new FormLayout();
    form.setMargin(false);
    form.setWidth("800px");
    form.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
    addComponent(form);

    Label section = new Label("Informa��es");
    section.addStyleName(ValoTheme.LABEL_H2);
    section.addStyleName(ValoTheme.LABEL_COLORED);
    form.addComponent(section);

    codigo = new TextField("C�digo");
    codigo.setWidth("50%");
    form.addComponent(codigo);

    time = new DateField("Hora");
    form.addComponent(time);

    username = new TextField("Usu�rio");
    form.addComponent(username);

    tipo = new TextField("Tipo");
    tipo.setWidth("50%");
    form.addComponent(tipo);

    section = new Label("Altera��o");
    section.addStyleName(ValoTheme.LABEL_H4);
    section.addStyleName(ValoTheme.LABEL_COLORED);
    form.addComponent(section);

    ttable = new TreeTable();
    ttable.addContainerProperty("Nome", String.class, null);
    ttable.addContainerProperty("Valor", Object.class, null);
    ttable.setWidth("100%");
    //ttable.setSizeFull();
    form.addComponent(ttable);

    form.setReadOnly(true);

    Button edit = new Button("Editar", event -> {
        boolean readOnly = form.isReadOnly();
        if (readOnly) {
          form.setReadOnly(false);
          form.removeStyleName(ValoTheme.FORMLAYOUT_LIGHT);
          event.getButton().setCaption("Salvar");
          event.getButton().addStyleName(ValoTheme.BUTTON_PRIMARY);
        }
        else {
          form.setReadOnly(true);
          form.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
          event.getButton().setCaption("Editar");
          event.getButton().removeStyleName(ValoTheme.BUTTON_PRIMARY);
        }
    });

    Button sair = new Button("Sair", event -> {
      Notification.show("Teste");
    });
    
    HorizontalLayout footer = new HorizontalLayout();
    footer.setMargin(new MarginInfo(true, false));
    footer.setSpacing(true);
    footer.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
    form.addComponent(footer);
    footer.addComponents(edit, sair);

    Label lastModified = new Label("Last modified by you a minute ago");
    lastModified.addStyleName(ValoTheme.LABEL_LIGHT);
    footer.addComponent(lastModified);
  }

  /**
   * Busca os hist�ricos ra�z e monta a lista de TreeNode ra�z para a �rvore de Hist�rico.
   * @return Lista de n�s ra�z.
   * @throws SQLException
   */
  TreeTable ttable;

  public void carregaRootList(String newLogValue) {
    try {
      PersistenceLogManager persistenceManager = new PersistenceLogManager();
      PersistenceObjectNode persistenceObject = persistenceManager.parse(newLogValue);
      Map<PersistencePropNode, PersistencePropNode> nodes = new HashMap<>();
      addToList(nodes,null, persistenceObject);

      nodes.forEach((key, value) -> ttable.setParent(key, value));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void addToList(Map<PersistencePropNode, PersistencePropNode> nodes,
                         PersistencePropNode parent,
                         PersistenceObjectNode node) {
    for (PersistencePropNode propNode : node.getPropNode()) {
      ttable.addItem(new Object[]{ propNode.getPropName(), propNode.getPropValue()}, propNode);
      nodes.put(propNode, parent);
      
      if(CollectionUtils.isNotEmpty(propNode.getObjectNode())) {
        for (PersistenceObjectNode component : propNode.getObjectNode()) {
          addToList(nodes, propNode, component);
        }
      }
    }
  }

  @Override
  public void enter(ViewChangeEvent event) {
    System.out.println(event);
    System.out.println(this);
  }
  
  @Subscribe
  public void setInfo(PostLogInfoEvent event) {
    LogInfo logInfo = event.getLogInfo();
    this.codigo.setValue(logInfo.getId());
    this.time.setValue(logInfo.getTime());
    this.tipo.setValue(logInfo.getDscLogType());
    this.username.setValue(logInfo.getUserId());
    carregaRootList(logInfo.getLogValue());
  }
}
