/**
 * 
 */
package com.logus.suporte.view;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import com.google.common.eventbus.Subscribe;
import com.logus.suporte.view.event.LogusEvent.BrowserResizeEvent;
import com.logus.suporte.view.event.LogusEvent.CloseOpenWindowsEvent;
import com.logus.suporte.view.event.LogusEvent.UserLoggedOutEvent;
import com.logus.suporte.view.event.LogusEventBus;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;


/**
 * @author Makino
 *
 */
@SpringUI
@Theme("logus")
@Title("LOGUS Suporte")
@SuppressWarnings("serial")
public class LogusUI extends UI {

  @Autowired
  private SpringViewProvider viewProvider;
  
  @Autowired
  private MainView mainView;

  @Autowired
  private LogusEventBus eventBus;
  
  @Override
  protected void init(VaadinRequest request) {
    //Adiciona o menu responsivo
    addStyleName(ValoTheme.UI_WITH_MENU);
    Responsive.makeResponsive(this);

    setContent(this.mainView);
    LogusEventBus.register(this);
   
    Page.getCurrent()
        .addBrowserWindowResizeListener(e -> LogusEventBus.post(new BrowserResizeEvent()));
    
    Navigator navigator = new LogusNavigator(this, this.mainView.getViewContainer());
    navigator.addProvider(viewProvider);
    setNavigator(navigator);
  }
  
  @Subscribe
  public void userLoggedOut(final UserLoggedOutEvent event) {
      VaadinSession.getCurrent().close();
      Page.getCurrent().reload();
      System.out.println("LOGOUT!!");
  }

  @Subscribe
  public void closeOpenWindows(final CloseOpenWindowsEvent event) {
      for (Window window : getWindows()) {
          window.close();
      }
  }
  
  public static LogusEventBus getEventBus() {
    return ( (LogusUI) getCurrent()).eventBus;
  }
  
}
